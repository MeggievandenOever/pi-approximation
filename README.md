# Pi Approximation

(define BOARDER (empty-scene 500 500))
(define MTS (rectangle 500 500 "outline" "black"))
(define CIRCLE (circle 250 "outline" "black"))
(define DOT (circle 4 "solid" "red"))

(define-struct dot [image x-coor y-coor number numberInC])
; A dot is a structure: (make-dot Image Number Number Number Number)
; Interpretation: image is an image of the specific dot placed in a square (possibly previous dots are displayed as well), x-coor is the
; x-coordinate of the dot where the origin is the left upper corner, x-coor is the x-coordinate of the dot where the origin is the left
; upper corner, number is the number of total dots displayed in the image, and numberInC is the number of dots that have radius less than 250
; where the origin is concidered to be the middle of the square (250, 250)

; Number -> string
; get-approx calculates the approximation of pi based on the number of dots with radius less than 250 (dot is in the circle) and number of
; dots with radius bigger than 250 (dot is outside the circle)
(define (get-approx p)
 (number->string (exact->inexact (* 4 (/ (dot-numberInC p) (dot-number p))))))
(check-expect (get-approx (make-dot (place-image DOT 250 250 MTS) 250 250 4 4)) 4.0)
(check-expect (get-approx (make-dot (place-image DOT 250 250 MTS) 250 250 18 9)) 2.0) 

; dot, Number, Number  -> Number
; get-numberInC takes in a dot (previous dot) and a x-coordinate and y-coordinate of the new dot, and calculates whether the radius of the new dot is
; bigger or smaller than 250 (inside or outside circle). If bigger, then number of dots in circle stays the same as it was for previous dot. If smaller
; , then number of dots in circle of previous dot is increased by one for the new dot.
(define (get-numberInC p x y)
 (if (<= (sqrt (+ (* (- 250 x) (- 250 x)) (* (- 250 y) (- 250 y)))) 250) (+ 1 (dot-numberInC p)) (dot-numberInC p)))
(check-expect (get-numberInC (make-dot (place-image DOT 250 250 MTS) 250 250 1 1) 250 250) 2)
(check-expect (get-numberInC (make-dot (place-image DOT 250 250 MTS) 250 250 1 1) 40 20) 1)

; dot -> dot
; reset-dot takes a random value under 500 for x and for y as local variables. Then it makes a new dot, for which the a dot is added to the image (overlay) with coordinates x and y.
; It stores the x value in the x-coor and the y value in y-coor, and it increases the number of total dots by one, and it increases the number of dots in cirlce by one if the
; magnitude of dot is smaller than 250 using the get-numberInC function
(define (reset-dot p)
  (local [(define x (random 500))
          (define y (random 500))]
     (make-dot (overlay (dot-image p)(place-image DOT x y MTS)) x y (+ 1 (dot-number p)) (get-numberInC p x y))))

; dot -> Image
; scene+dot adds the image variable of the dot into the BOARDER and CIRCLE
(define (scene+dot p)
  (overlay (overlay/xy CIRCLE 250 100(text (get-approx p) 16 "black")) CIRCLE (dot-image p) BOARDER))

(define (to-state p) (dot-y-coor p))
(define (main p)
  (big-bang p
    [on-tick reset-dot]
    [to-draw scene+dot]
    [state (to-state p)]))

(main (make-dot (place-image DOT 250 250 MTS) 250 250 1 1))
